const controller = require('../controllers/bookstore');

function bookstoreRoutes(app) {
  app.post('/bookstore', controller.postBookstore);

  app.get('/bookstore', controller.getBookstore);

  app.get('/bookstore/:bookstoreID', controller.getBookstoreByID);

  app.delete('/bookstore/:bookstoreID', controller.deleteBookstoreByID);

  app.put('/bookstore/:bookstoreID', controller.putBookstoreByID);
}

module.exports = bookstoreRoutes;
