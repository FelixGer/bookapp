const controller = require('../controllers/book');

function bookRoutes(app) {
  app.post('/book', controller.postBook);

  app.get('/book', controller.getAll);

  app.get('/books/:bookstoreID', controller.getByBookstoreID);

  app.get('/book/:bookID', controller.getByBookID);

  app.delete('/book/:bookID', controller.deleteByBookID);

  app.put('/book/:bookID', controller.putById);

  app.post('/book/buy', controller.buyByID);
}

module.exports = bookRoutes;
