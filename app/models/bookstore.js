const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BookstoreSchema = new Schema({
  name: { type: String, required: true, unique: true },
  address: {
    street: { type: String },
    number: { type: Number },
    country: { type: String, default: 'Spain' },
    zip: { type: Number },
    town: String,
  },
});


BookstoreSchema.index({ name: 1 });
BookstoreSchema.index({ addresses: 1 });
BookstoreSchema.index({ country: 1 });

module.exports = mongoose.model('bookstore', BookstoreSchema);
