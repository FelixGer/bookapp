const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BookSchema = new Schema(
  {
    bookstore: { type: Schema.ObjectId, required: true, ref: 'bookstore' },
    title: { type: String, required: true },
    author: { type: String, required: true },
    year: { type: Number, required: true },
    pages: { type: Number },
    createdAt: { type: Date, default: Date.now },
    availability: { type: Number, default: 1 },
  },
  {
    versionKey: false,
  });

// Sets the createdAt parameter equal to the current time
BookSchema.pre('save', (next) => {
  const now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

BookSchema.index({ bookstore: 1 });
BookSchema.index({ title: 1 });
BookSchema.index({ createdAt: 1 });
BookSchema.index({ author: 1, title: 1 });


// Exports the BookSchema for use elsewhere.
module.exports = mongoose.model('book', BookSchema);
