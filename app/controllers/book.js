const Book = require('../models/book');
const co = require('co');

function getAll(req, res) {
  co(function* generator() {
    const books = yield Book.find({}).exec();
    res.send(books);
  }).catch((err) => {
    res.send(err);
  });
}

function getByBookstoreID(req, res) {
  co(function* generator() {
    const book = yield Book.find({ bookstore: req.params.bookstoreID }).exec();
    res.send(book);
  }).catch((err) => {
    res.send(err);
  });
}


function getByBookID(req, res) {
  co(function* generator() {
    const book = yield Book.find({ _id: req.params.bookID }).exec();
    res.send(book);
  }).catch((err) => {
    res.send(err);
  });
}

function deleteByBookID(req, res) {
  co(function* generator() {
    const book = yield Book.findByIdAndRemove(req.params.bookID).exec();
    res.send(book);
  }).catch((err) => {
    res.send(err);
  });
}

function putById(req, res) {
  co(function* generator() {
    const book = yield Book.findByIdAndUpdate(req.params.bookID, req.body, { new: true }).exec();
    res.send(book);
  }).catch((err) => {
    res.send(err);
  });
}

function postBook(req, res) {
  const newBook = new Book(req.body);
  co(function* generator() {
    const book = yield newBook.save();
    res.send(book);
  }).catch((err) => {
    res.send(err);
  });
}


function buyByID(req, res) {
  co(function* generator() {
    const bookid = req.body.bookid;
    const amount = req.body.amount;

    const queryBook = {
      _id: bookid,
    };

    const book = yield Book.findById(queryBook).lean().exec();

    if (!book) {
      res.send('El libro no se ha encontrado en la base de datos');
      return;
    }

    if (book.availability < amount) {
      return;
    }
    book.availability -= amount;

    const updatedBook = yield Book.findByIdAndUpdate(queryBook, book, { new: true }).lean().exec();

    res.send(updatedBook);
  });
}


module.exports = {
  getAll, getByBookstoreID, getByBookID, deleteByBookID, putById, postBook, buyByID,
};
