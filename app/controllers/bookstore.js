const Bookstore = require('../models/bookstore.js');
const co = require('co');

function postBookstore(req, res) {
  const newBookstore = new Bookstore(req.body);
  co(function* generator() {
    const bookstore = yield newBookstore.save();
    res.send(bookstore);
  }).catch((err) => {
    res.send(err);
  });
}

function getBookstore(req, res) {
  co(function* generator() {
    const bookstore = yield Bookstore.find({}).exec();
    res.send(bookstore);
  }).catch((err) => {
    res.send(err);
  });
}

function getBookstoreByID(req, res) {
  co(function* generator() {
    const bookstore = yield Bookstore.findById(req.params.bookstoreID).exec();
    res.send(bookstore);
  }).catch((err) => {
    res.send(err);
  });
}

function deleteBookstoreByID(req, res) {
  co(function* generator() {
    const bookstore = yield Bookstore.findByIdAndRemove(req.params.bookstoreID).exec();
    res.send(bookstore);
  }).catch((err) => {
    res.send(err);
  });
}

function putBookstoreByID(req, res) {
  co(function* generator() {
    const bookstore = yield Bookstore.findByIdAndUpdate(
      req.params.bookstoreID, req.body, { new: true }).exec();
    res.send(bookstore);
  }).catch((err) => {
    res.send(err);
  });
}


module.exports = {
  postBookstore, getBookstore, getBookstoreByID, deleteBookstoreByID, putBookstoreByID,
};
